class PromiseHolder<T = void> {
  readonly catch: Promise<T>['catch'];

  readonly createdAt = new Date();

  readonly nodeCallback: NodeCallback<T>;

  readonly promise: Promise<T>;

  readonly reject: (reason: unknown) => void = resolveUnknownError;

  readonly resolve: (value: T) => void;

  readonly then: Promise<T>['then'];

  constructor() {
    let resolve = resolveUnknownError<T>;
    let reject = resolveUnknownError;

    this.promise = new Promise<T>((resolveCb, rejectCb) => {
      resolve = resolveCb as ResolveCb<T>;
      reject = rejectCb;
    });
    this.resolve = resolve;
    this.reject = reject;
    this.then = this.promise.then.bind(this.promise);
    this.catch = this.promise.catch.bind(this.promise);

    this.nodeCallback = ((err: unknown, value: T) => {
      if (err) {
        this.reject(err);
      } else {
        this.resolve(value);
      }
    }) as NodeCallback<T>;
  }

  async inTime(duration: number): Promise<boolean> {
    const endPromise = new PromiseHolder<boolean>();

    const timer = setTimeout(
      () => {
        endPromise.resolve(false);
      },
      duration,
    );

    const inTimeCb = () => {
      endPromise.resolve(true);
      clearTimeout(timer);
    };

    void this.then(inTimeCb, inTimeCb);

    const inTime = await endPromise;

    return inTime;
  }
}

type NodeCallback<T, E = unknown> = T extends undefined
  ? (err: E) => void
  : (err: E, value: T) => void;

type ResolveCb<T> = T extends undefined
  ? () => void
  : (value: T) => void;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function resolveUnknownError<T>(value: T): void {
  throw new Error('Unexpected error');
}

export default PromiseHolder;
