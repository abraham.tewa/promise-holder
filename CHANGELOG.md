## [1.0.2](https://gitlab.com/abraham.tewa/promise-holder/compare/v1.0.1...v1.0.2) (2024-07-08)


### Bug Fixes

* **types:** Include type declaration file in package ([241e30c](https://gitlab.com/abraham.tewa/promise-holder/commit/241e30cdb89a4e1a50c3d0c741fb87ae170a4a80))

## [1.0.1](https://gitlab.com/abraham.tewa/promise-holder/compare/v1.0.0...v1.0.1) (2024-02-25)


### Bug Fixes

* Add documentation ([0629ccc](https://gitlab.com/abraham.tewa/promise-holder/commit/0629ccc8a49297f09855e954e5521cd514b27b41))

# 1.0.0 (2024-02-25)


### Features

* Create PromiseHolder class ([fa78164](https://gitlab.com/abraham.tewa/promise-holder/commit/fa7816497affa91e591c791e7807cde6fd33c780))
